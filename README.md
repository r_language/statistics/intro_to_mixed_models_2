# Second_Intro_to_mixed_models


In the R file provided, there are some operation using mixed models :


- Student test (t.test)


- One way anova (to summarize the analysis of variance model) using aov function


- Anova


- Linear regression with lm function


- Linear mixed effects models with lme function


- The use of lmer function to fit a linear mixed-effects model (LMM) to data


Author : Marion Estoup

E-mail : marion_110@hotmail.fr

November 2021
